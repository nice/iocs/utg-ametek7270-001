#!/usr/bin/env iocsh.bash

require(ametek7270)
#require(streamdevice)
#require(asyn)
#require(calc)
#require(utgard_base)


# -----------------------------------------------------------------------------
# Default environment variables
# -----------------------------------------------------------------------------
epicsEnvSet("IPADDR",   "172.30.244.79")
epicsEnvSet("IPPORT",   "50000")
epicsEnvSet("LOCATION", "Utgard; $(IPADDR)")
epicsEnvSet("SYS",      "UTG-SEE-TEFI:")
epicsEnvSet("DEV",      "LockIn-AMETEK-01")
epicsEnvSet("PREFIX",   "$(SYS)$(DEV)")
#epicsEnvSet("IOCNAME",  "$(SYS)AMETEK")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", 1000000)


# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------

iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# Ametek 7270
iocshLoad("$(ametek7270_DIR)/ametek7270.iocsh", "PREFIX=$(PREFIX), SYS=$(SYS), DEV=$(DEV), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")

iocInit()
